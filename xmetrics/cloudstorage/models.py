from django.db import models

class CloudStorageStatus(models.Model):
    name = models.CharField(max_length=50)

class CloudStorageType(models.Model):
    name = models.CharField(max_length=50)

class CloudStorage(models.Model):
    cs_id = models.CharField(max_length=9, unique=True)
    name = models.CharField(max_length=50)
    account_email = models.EmailField()
    account_password = models.CharField(max_length=50)
    last_update = models.DateTimeField(null=True)
    used_storage = models.CharField(max_length=10, null=True)

    cloudstorage_status = models.ForeignKey(CloudStorageStatus, on_delete=models.CASCADE)
    cloudstorage_type = models.ForeignKey(CloudStorageType, on_delete=models.CASCADE)