from django.forms import ModelForm
from xmetrics.cloudstorage.models import CloudStorage

class CloudStorageForm(ModelForm):
    class Meta:
        model = CloudStorage
        fields = ['account_email', 'account_password']