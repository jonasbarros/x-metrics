from django.shortcuts import render
from xmetrics.cloudstorage.models import CloudStorage, CloudStorageStatus
from xmetrics.cloudstorage.forms import CloudStorageForm
from base64 import b64encode

def xmetrics_cloudstorage(request):
    cloudstorage_default = CloudStorage.objects.get(id=1)

    if request.method == 'POST':
        form = CloudStorageForm(request.POST, instance=cloudstorage_default)
        if form.is_valid():
            cloudstorage_default.cloudstorage_status = CloudStorageStatus.objects.get(id=1)
            cloudstorage_default.account_email = request.POST['account_email']
            cloudstorage_default.account_password = b64encode(str.encode(request.POST['account_password'])).decode()
            cloudstorage_default.save()

    return render(request, 'xmetrics_cloudstorage.html', {
        'cloudstorage' : cloudstorage_default
    })