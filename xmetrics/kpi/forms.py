from django.forms import ModelForm
from xmetrics.kpi.models import Kpi

class KpiForm(ModelForm):
    class Meta:
        model = Kpi
        fields = ['name', 'worksheet', 'sheet', 'cell', 'kpi_type']