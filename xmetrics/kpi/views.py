from django.shortcuts import render, redirect
from xmetrics.kpi.models import Kpi, KpiType, KpiStatus
from xmetrics.cloudstorage.models import CloudStorage
from xmetrics.kpi.forms import KpiForm
from datetime import datetime
from hashlib import sha256
from random import randint
import re

def xmetrics_kpi(request):
    return render(request, 'xmetrics_kpi.html', {
        'kpi': Kpi.objects.all()
    })

def xmetrics_kpi_form(request):
    kpi_id_mask = re.compile('kpi-[0-9a-z][0-9a-z][0-9a-z][0-9a-z][0-9a-z][0-9a-z]')

    if request.GET.get('kpi_id', False) and kpi_id_mask.match(request.GET.get('kpi_id')):
        kpi = Kpi.objects.get(kpi_id=request.GET.get('kpi_id'))
    else:
        kpi = None

    if request.method == 'POST':
        form = KpiForm(request.POST, instance=kpi)
        if form.is_valid():
            if kpi:
                kpi.name = request.POST['name']
                kpi.worksheet = request.POST['worksheet']
                kpi.sheet = request.POST['sheet']
                kpi.cell = request.POST['cell']
                kpi.kpi_type = KpiType.objects.get(id=request.POST['kpi_type'])
                kpi.kpi_status = KpiStatus.objects.get(id=1)
                kpi.save()
            if not kpi:
                Kpi.objects.create(
                    name=request.POST['name'],
                    worksheet=request.POST['worksheet'],
                    sheet=request.POST['sheet'],
                    cell=request.POST['cell'],
                    kpi_type=KpiType.objects.get(id=request.POST['kpi_type']),
                    kpi_status=KpiStatus.objects.get(id=1),
                    cloudstorage = CloudStorage.objects.get(id=1),
                    kpi_id='kpi-{hash}'.format(hash=sha256(str((randint(0, 999999999), datetime.now())).encode('utf-8')).hexdigest()[:6])
                )
            return redirect('xmetrics_kpi')

    return render(request, 'xmetrics_kpi_form.html', {
        'kpi_type': KpiType.objects.all(),
        'kpi': kpi
    })

def xmetrics_kpi_query(request):
    return render(request, 'xmetrics_kpi_query.html', {
        'kpi': Kpi.objects.get(kpi_id=request.GET.get('kpi_id'))
    })

def xmetrics_kpi_remove(request):
    if request.method == 'POST':
        Kpi.objects.get(kpi_id=request.GET.get('kpi_id')).delete()
        return redirect('xmetrics_kpi')

    return render(request, 'xmetrics_kpi_remove.html', {
        'kpi': Kpi.objects.get(kpi_id=request.GET.get('kpi_id'))
    })