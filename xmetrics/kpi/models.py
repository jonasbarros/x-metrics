from django.db import models
from xmetrics.cloudstorage.models import CloudStorage

class KpiStatus(models.Model):
    name = models.CharField(max_length=50)

class KpiType(models.Model):
    name = models.CharField(max_length=50)
    query = models.CharField(max_length=10000)

class Kpi(models.Model):
    kpi_id = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=50)
    worksheet = models.CharField(max_length=255)
    sheet = models.CharField(max_length=50)
    cell = models.CharField(max_length=10)
    kpi_status = models.ForeignKey(KpiStatus, on_delete=models.CASCADE)
    kpi_type = models.ForeignKey(KpiType, on_delete=models.CASCADE)
    cloudstorage = models.ForeignKey(CloudStorage, on_delete=models.CASCADE)

    def get_query(self):
        return self.kpi_type.query.replace('{{ kpi_id }}', self.kpi_id)