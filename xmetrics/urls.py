"""xmetrics URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from xmetrics.kpi import views as kpi
from xmetrics.cloudstorage import views as cloudstorage

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', kpi.xmetrics_kpi, name='xmetrics_kpi'),
    path('xmetrics_kpi_form/', kpi.xmetrics_kpi_form, name='xmetrics_kpi_form'),
    path('xmetrics_kpi_query/', kpi.xmetrics_kpi_query, name='xmetrics_kpi_query'),
    path('xmetrics_kpi_remove/', kpi.xmetrics_kpi_remove, name='xmetrics_kpi_remove'),
    path('xmetrics_cloudstorage/', cloudstorage.xmetrics_cloudstorage, name='xmetrics_cloudstorage')
]
